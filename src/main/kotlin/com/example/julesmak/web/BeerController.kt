package com.example.julesmak.web

import com.example.julesmak.persistence.JulesmakVurdering
import com.example.julesmak.persistence.JulesmakVurderingRepository
import org.springframework.core.ParameterizedTypeReference
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component
import org.springframework.util.MultiValueMap
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.reactive.function.client.WebClient

@RestController
class BeerController(private val punkApiWebClient: WebClient,
                     private val julesmakVurderingRepository: JulesmakVurderingRepository) {
    @GetMapping
    fun getBeers(@RequestParam params: MultiValueMap<String, String>) =
            punkApiWebClient.get()
                    .uri { it.queryParams(params).build() }
                    .retrieve()
                    .bodyToMono(object: ParameterizedTypeReference<List<Map<String, Any?>>>() {})
                    .block()
                    ?.map {
                        it.plus("julesmakKarakter" to getJulesmakVurderingForBeer(it)?.terningKast)
                    }

    @PutMapping("/{id}/julesmakKarakter")
    fun saveMattilsynetsVurdering(@PathVariable id: Int, @RequestBody terningKast: Int) =
            julesmakVurderingRepository.save(JulesmakVurdering(id = id, terningKast = terningKast))

    private fun getJulesmakVurderingForBeer(beerPropertiesMap: Map<String, Any?>) =
            (beerPropertiesMap.get("id") as? Int)?.let { julesmakVurderingRepository.findByIdOrNull(it) }
}
