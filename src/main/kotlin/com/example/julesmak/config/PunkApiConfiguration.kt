package com.example.julesmak.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.client.WebClient

@Configuration
class PunkApiConfiguration {
    @Bean
    fun punkApiWebClient() = WebClient.builder()
            .baseUrl("https://api.punkapi.com/v2/beers")
            .build()
}
