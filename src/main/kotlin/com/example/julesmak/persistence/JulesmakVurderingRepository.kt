package com.example.julesmak.persistence

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface JulesmakVurderingRepository: CrudRepository<JulesmakVurdering, Int>
