package com.example.julesmak.persistence

import org.springframework.validation.annotation.Validated
import javax.persistence.Entity
import javax.persistence.Id
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

@Entity
@Validated
data class JulesmakVurdering(@NotNull @Id val id: Int, @Max(6) @Min(1) val terningKast: Int)
